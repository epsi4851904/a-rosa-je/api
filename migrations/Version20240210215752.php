<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240210215752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, posted_by_id INT DEFAULT NULL, keeped_by_id INT DEFAULT NULL, name_plant VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, start_at DATETIME DEFAULT NULL, end_at DATETIME DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(255) DEFAULT NULL, keeping_validate TINYINT(1) DEFAULT 0 NOT NULL, note INT DEFAULT NULL, INDEX IDX_5A8A6C8D5A6D2235 (posted_by_id), INDEX IDX_5A8A6C8DB21493E0 (keeped_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D5A6D2235 FOREIGN KEY (posted_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DB21493E0 FOREIGN KEY (keeped_by_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D5A6D2235');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8DB21493E0');
        $this->addSql('DROP TABLE post');
    }
}
