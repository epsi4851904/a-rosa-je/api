<?php

namespace App\Repository;

use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Message>
 *
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function getMessages(User $recipient, User $sender)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.sender = :sender and m.recipient = :recipient')
            ->orWhere('m.sender = :recipient and m.recipient = :sender')
            ->setParameters([
                'recipient' => $recipient,
                'sender' => $sender
            ])
            ->orderBy('m.sendAt', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getLastMessage(User $recipient, User $sender)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.sender = :sender and m.recipient = :recipient')
            ->orWhere('m.sender = :recipient and m.recipient = :sender')
            ->setParameters([
                'recipient' => $recipient,
                'sender' => $sender
            ])
            ->orderBy('m.sendAt', 'DESC')
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getAllMessage(User $user)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('u.id')
            ->leftJoin('m.recipient', 'u', 'WITH', 'm.recipient = u OR m.sender = u')
            ->where('m.sender = :userId OR m.recipient = :userId')
            ->groupBy('u.id')
            ->having('u.id != :userId')
            ->setParameters(['userId' => $user->getId()]);

        $dql = '
            SELECT u.id
            FROM '. Message::class .' m
            LEFT JOIN '. User::class .' u WITH m.recipient = u.id OR m.sender = u.id
            WHERE m.recipient = :userId or m.sender = :userId
            GROUP BY u.id
            HAVING u.id != :userId
        ';
        $qb = $this->getEntityManager()->createQuery($dql);
        $qb->setParameters(['userId' => $user->getId()]);

        return $qb->getResult();
    }
}
