<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getPosts($user, string $search = '')
    {
        $qb = $this->createQueryBuilder('p');
        $qb->leftJoin('p.comments', 'c')
            ->leftJoin('p.postedBy', 'u')
            ->where('p.deletedAt IS NULL')
            ->andWhere('u.deletedAt IS NULL')
            ->andWhere('c.deletedAt IS NULL')
            ->andWhere('p.postedBy <> :user')
            ->setParameter('user', $user)
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('p.namePlant', ':search'),
                    $qb->expr()->like('p.description', ':search'),
                    $qb->expr()->like('p.zipcode', ':search'),
                    $qb->expr()->like('p.city', ':search'),
                    $qb->expr()->like('u.firstName', ':search'),
                    $qb->expr()->like('u.lastName', ':search')
                )
            )
            ->setParameter('search', '%' . $search . '%');

        return $qb->getQuery()->getResult();
    }

    public function getPostForMap($user)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->leftJoin('p.images', 'i')
            ->leftJoin('p.comments', 'c')
            ->where('p.deletedAt IS NULL')
            ->andWhere('i.deletedAt IS NULL')
            ->andWhere('c.deletedAt IS NULL')
            ->andWhere('p.deletedAt IS NULL')
            ->andWhere('i.isMain = 1')
            ->andWhere('p.latitude IS NOT NULL')
            ->andWhere('p.postedBy <> :user')
            ->setParameter('user', $user)
            ->andWhere('p.longitude IS NOT NULL');

        return $qb->getQuery()->getResult();
    }

    public function getPostWithActionByUser(User $user)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.keepedBy = :user')
            ->andWhere('p.keepingValidate = 0')
            ->andWhere('p.deletedAt IS NULL')
            ->setParameter('user', $user->getId());

        return $qb->getQuery()->getResult();
    }
}
