<?php

namespace App\Service;

use Google\Client;
use Google\Service\Drive;
use Google\Service\Drive\DriveFile;

class GoogleApiService
{
    private Client $clientGoogle;

    public function __construct()
    {
        $this->clientGoogle = new Client();
        $this->clientGoogle->setAuthConfig('./credentials.json');
        $this->clientGoogle->addScope(Drive::DRIVE);
    }

    public function uploadFile(string $filePath, string $filename): DriveFile
    {
        $driveService = new Drive($this->clientGoogle);

        $file = new Drive\DriveFile([
            'name' => $filename,
            'parents' => ['1d3rcSHATKmqoyMQAl0omvBckvivKvJYm']
        ]);

        $content = file_get_contents($filePath);
        $fileUpload = $driveService->files->create($file, [
            'data' => $content,
            'mimeType' => 'text/csv',
            'uploadType' => 'multipart'
        ]);

        return $fileUpload;
    }
}
