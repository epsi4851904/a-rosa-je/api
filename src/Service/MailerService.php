<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class MailerService
{
    private TemplatedEmail $email;

    public function __construct(private readonly string $emailFrom, private readonly MailerInterface $mailer)
    {
        $this->email = new TemplatedEmail();
    }

    public function createEmail(string $to, string $subject): static
    {
        $this->email->from($this->emailFrom)
            ->to($to)
            ->subject($subject);
        return $this;
    }

    /**
     * @param string $template
     * @param array<mixed> $context
     * @return $this
     */
    public function setBodyWithTemplate(string $template, array$context = []): static
    {
        $this->email->htmlTemplate($template);
        $this->email->context($context);

        return $this;
    }

    public function addAttachments($file, $filename): static
    {
        $this->email->attachFromPath($file, $filename);

        return $this;
    }

    public function sendEmail(): void
    {
        try {
            $this->mailer->send($this->email);
        } catch (TransportExceptionInterface $e) {
            echo 'Erreur lors de l\'envoi de l\'e-mail : ' . $e->getMessage();
        }
    }
}
