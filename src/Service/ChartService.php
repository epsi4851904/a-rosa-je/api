<?php

namespace App\Service;

use App\Service\Chart\ChartInterface;
use App\Service\Chart\Monitoring\AllRequestTime;
use App\Service\Chart\Monitoring\NbRequestByMethod;
use App\Service\Chart\Monitoring\TimeRequestByMethod;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class ChartService
{
    private array $charts;

    public function __construct(
        private readonly ChartBuilderInterface $chartBuilder,
        private readonly TimeRequestByMethod $timeRequestByMethod,
        private readonly NbRequestByMethod $nbRequestByMethod,
        private readonly AllRequestTime $allRequestTime
    ) {
        $this->charts = compact('timeRequestByMethod', 'nbRequestByMethod', 'allRequestTime');
    }

    public function getChart(string $chartName): Chart
    {
        /** @var ChartInterface $chartManagement */
        $chartManagement = $this->charts[$chartName];
        $chart = $this->chartBuilder->createChart($chartManagement->getType());
        $chart->setData([
            'labels' => $chartManagement->getData()['label'],
            'datasets' => [
                [
                    'label' => $chartManagement->getTitle(),
                    'backgroundColor' => $chartManagement->getBackgroundColor(),
                    'borderColor' => 'rgba(255, 205, 86)',
                    'data' => $chartManagement->getData()['data']
                ]
            ]
        ]);

        $chart->setOptions([
            'plugins' => [
                'zoom' => [
                    'zoom' => [
                        'wheel' => ['enabled' => true],
                        'pinch' => ['enabled' => true],
                        'mode' => 'xy',
                    ],
                ],
            ],
        ]);

        return $chart;
    }
}
