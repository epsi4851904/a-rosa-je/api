<?php

namespace App\Service\Chart;

interface ChartInterface
{
    public function getTitle(): string;

    public function getType(): string;

    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @return array
     */
    public function getBackgroundColor(): array;
}
