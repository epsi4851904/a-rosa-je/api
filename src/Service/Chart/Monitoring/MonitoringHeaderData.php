<?php

namespace App\Service\Chart\Monitoring;

class MonitoringHeaderData
{
    private int $nbRequest = 0;
    private float $avgRequestTime = 0;
    private int $nbErrorsRequest = 0;

    public function __construct(private readonly string $csvFilename)
    {
    }

    public function getData(): array
    {
        if (file_exists($this->csvFilename)) {
            if (($handle = fopen($this->csvFilename, "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    if ($data[0] !== 'Date') {
                        if (str_contains($data[1], 'api')) {
                            $this->nbRequest++;
                            $this->avgRequestTime += floatval($data[5]);
                            if (intval($data[4]) >= 500) {
                                $this->nbErrorsRequest++;
                            }
                        }
                    }
                }
                fclose($handle);
            }
        }

        $this->avgRequestTime = $this->nbRequest > 0 ? $this->avgRequestTime / $this->nbRequest : 0;

        return [
            'nbRequest' => $this->nbRequest,
            'avgRequestTime' => round($this->avgRequestTime, 2),
            'nbErrorsRequest' => $this->nbErrorsRequest
        ];
    }
}
