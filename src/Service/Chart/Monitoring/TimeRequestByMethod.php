<?php

namespace App\Service\Chart\Monitoring;

use App\Service\Chart\AbstractChart;
use App\Service\Chart\ChartInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\Chartjs\Model\Chart;

class TimeRequestByMethod extends AbstractChart implements ChartInterface
{
    public function __construct(EntityManagerInterface $entityManager, private readonly string $csvFilename)
    {
        parent::__construct($entityManager);
    }

    public function getTitle(): string
    {
        return 'Temps de réponse par méthode';
    }

    public function getType(): string
    {
        return Chart::TYPE_BAR;
    }

    public function getData(): array
    {
        $methods = ['GET', 'POST', 'DELETE'];
        $methodData = [];

        foreach ($methods as $method) {
            $totalTime = 0;
            $totalCount = 0;
            if (file_exists($this->csvFilename)) {
                if (($handle = fopen($this->csvFilename, "r")) !== false) {
                    while (($data = fgetcsv($handle)) !== false) {
                        if ($data[0] !== 'Date') {
                            if (str_contains($data[1], 'api')) {
                                if ($data[3] === $method) {
                                    $totalTime += $data[5];
                                    $totalCount++;
                                }
                            }
                        }
                    }
                    fclose($handle);
                }
            }

            $averageTime = $totalCount > 0 ? $totalTime / $totalCount : 0;

            $methodData[$method] = $averageTime;
        }

        return [
            'label' => $methods,
            'data' => $this->mapData($methodData)
        ];
    }

    public function mapData(array $data): array
    {
        $map = [];
        foreach ($data as $key => $item) {
            $map[] = $data[$key];
        }
        return $map;
    }

    public function getBackgroundColor(): array
    {
        return [
            'rgba(255, 205, 86)',
            'rgba(205, 92, 92)',
            'rgba(255, 205, 86)'
        ];
    }
}
