<?php

namespace App\Service\Chart\Monitoring;

use App\Service\Chart\AbstractChart;
use App\Service\Chart\ChartInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\Chartjs\Model\Chart;

class AllRequestTime extends AbstractChart implements ChartInterface
{
    public function __construct(EntityManagerInterface $entityManager, private readonly string $csvFilename)
    {
        parent::__construct($entityManager);
    }

    public function getTitle(): string
    {
        return 'Temps de réponse de toutes les requêtes';
    }

    public function getType(): string
    {
        return Chart::TYPE_LINE;
    }

    public function getData(): array
    {
        $requests = [];
        if (file_exists($this->csvFilename)) {
            if (($handle = fopen($this->csvFilename, "r")) !== false) {
                while (($data = fgetcsv($handle)) !== false) {
                    if ($data[0] !== 'Date') {
                        if (str_contains($data[1], 'api')) {
                            $requests[$data[0]] = $data[5];
                        }
                    }
                }
                fclose($handle);
            }
        }

        return [
            'label' => $this->mapLabel($requests),
            'data' => $this->mapData($requests)
        ];
    }

    public function getBackgroundColor(): array
    {
        return [
            'rgba(255, 205, 86)',
        ];
    }

    public function mapLabel(array $data): array
    {
        $map = [];
        foreach ($data as $timestamp => $value) {
            $dateTime = new DateTime($timestamp);
            $timeFormatted = $dateTime->format('H:i');
            $map[] = $timeFormatted;
        }
        return $map;
    }

    public function mapData(array $data): array
    {
        $map = [];
        foreach ($data as $key => $item) {
            $map[] = $data[$key];
        }
        return $map;
    }
}
