<?php

namespace App\Command;

use App\Service\GoogleApiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCsvLogsAPIDriveGoogleCommand extends Command
{
    protected static $defaultName = 'app:send-csv-to-drive';

    protected function configure()
    {
        $this->setDescription('Envoyer un fichier CSV vers Google Drive');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $googleDriveService = new GoogleApiService();
        $date = (new \DateTime())->format('Y-m-d');
        $filename = 'logs_'. $date . '.csv';
        $filePath = './var/log/requests.csv';
        $googleDriveService->uploadFile($filePath, $filename);

        $output->writeln('Le fichier ' . $filename . ' a été envoyé avec succès vers Google Drive.');
        if (unlink($filePath)) {
            $output->writeln('Le fichier a été supprimé avec succès.');
        } else {
            $output->writeln('Une erreur s\'est produite lors de la suppression du fichier.');
        }

        return Command::SUCCESS;
    }
}
