<?php

namespace App\Controller\Api;

use App\Entity\Image;
use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

#[Route('/post')]
class PostController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PostRepository $postRepository,
        private readonly NormalizerInterface $normalizer,
        private readonly UploaderHelper $helper,
        private readonly MailerService $mailerService,
        private readonly UserRepository $userRepository
    ) {
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('', name: 'get_posts', methods: ['GET'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function list(Request $request): JsonResponse
    {
        $search = $request->query->get('search') ?? '';
        $posts = $this->postRepository->getPosts($this->getUser(), $search);

        foreach ($posts as &$post) {
            foreach ($post->getImages() as $image) {
                /** @var Image $image */
                if ($image->getIsMain()) {
                    $image->setName($this->helper->asset($image, 'file'));
                    $post->addImage($image);
                }
            }
        }

        return new JsonResponse(
            $this->normalizer->normalize($posts, 'array', [
                'groups' => 'post'
            ]),
            Response::HTTP_OK
        );
    }

    #[Route('/map', name: 'get_post_for_map', methods: ['GET'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function getPostMap(): JsonResponse
    {
        $posts = $this->postRepository->getPostForMap($this->getUser());

        foreach ($posts as &$post) {
            foreach ($post->getImages() as $image) {
                /** @var Image $image */
                if ($image->getIsMain()) {
                    $image->setName($this->helper->asset($image, 'file'));
                    $post->addImage($image);
                }
            }
        }

        return new JsonResponse(
            $this->normalizer->normalize($posts, 'json', ['groups' => ['post', 'comment']]),
            Response::HTTP_OK
        );
    }

    #[Route('', name: 'post_create', methods: ['POST'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function create(Request $request): JsonResponse
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid() && $request->files->get('image')) {
            $image = new Image();
            $image->setFile($request->files->get('image'))
                ->setIsMain(true);
            $post->addImage($image);
            $post->setPostedBy($this->getUser());

            $this->entityManager->persist($image);
            $this->entityManager->flush();

            return new JsonResponse(['message' => 'Le post a bien été créée'], Response::HTTP_CREATED);
        }

        return new JsonResponse(['message' => 'Une erreure est survenue'], Response::HTTP_CONFLICT);
    }

    #[Route('/{post}', name: 'get_post_by_id', methods: ['GET'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function getPostById(Post $post): JsonResponse
    {
        foreach ($post->getImages() as $image) {
            $image->setName($this->helper->asset($image, 'file'));
            $post->addImage($image);
        }
        return new JsonResponse(
            $this->normalizer->normalize($post, 'json', ['groups' => ['post', 'comment']]),
            Response::HTTP_OK
        );
    }

    #[Route('/{post}', name: 'update_post_by_id', methods: ['POST'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function updatePostById(Request $request, Post $post): JsonResponse
    {
        $form = $this->createForm(PostType::class, $post);
        $form->submit($request->request->all(), false);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();
        }
        return new JsonResponse(
            $this->normalizer->normalize($post, 'json', ['groups' => 'post']),
            Response::HTTP_OK
        );
    }

    #[Route('/{post}', name: 'delete_post_by_id', methods: ['DELETE'])]
    /**
     * @OA\Tag(name="Post")
     */
    public function deletePostById(Post $post): JsonResponse
    {
        $post->setDeletedAt(new \DateTime());

        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return new JsonResponse(['message' => 'Post supprimé avec succès'], Response::HTTP_OK);
    }

    #[Route('/{post}/add-image', name: 'add_image_to_post', methods: ['POST'])]
    /**
     * @OA\Tag(name="Post - Image")
     */
    public function addImageToPost(Request $request, Post $post): JsonResponse
    {
        if (!$request->files->get('image')) {
            return new JsonResponse(['message' => 'Image manquante'], Response::HTTP_CONFLICT);
        }
        if ($this->getUser() !== $post->getKeepedBy() || ($this->getUser() === $post->getKeepedBy() && !$post->isKeepingValidate())) {
            return new JsonResponse(['message' => "Vous n'êtes pas autorisé à ajouter une image à ce poste"], Response::HTTP_CONFLICT);
        }
        $image = new Image();
        $image->setFile($request->files->get('image'));
        $post->addImage($image);

        $botanistes = $this->userRepository->getBotanistCommentPost($post->getId());

        foreach ($botanistes as $botaniste) {
            $this->mailerService->createEmail($botaniste->getEmail(), "AJOUT D'UNE IMAGE SUR UN POST");
            $this->mailerService->setBodyWithTemplate('emails/posts/add_image_botaniste.html.twig', compact('post', 'botaniste'));
            $this->mailerService->sendEmail();
        }

        $this->entityManager->persist($image);
        $this->entityManager->flush();

        $user = $this->getUser();
        $this->mailerService->createEmail($post->getPostedBy()->getEmail(), "AJOUT D'UNE IMAGE SUR VOTRE POST");
        $this->mailerService->setBodyWithTemplate('emails/posts/add_image.html.twig', compact('post', 'user'));
        $this->mailerService->sendEmail();

        return new JsonResponse(['message' => 'Image ajoutée avec succès'], Response::HTTP_OK);
    }

    #[Route('/{image}/delete-image', name: 'delete_image_by_id', methods: ['DELETE'])]
    /**
     * @OA\Tag(name="Post - Image")
     */
    public function deleteImageById(Image $image): JsonResponse
    {
        $image->setDeletedAt(new \DateTime());

        $this->entityManager->persist($image);
        $this->entityManager->flush();

        return new JsonResponse(['message' => 'Image deleted with success'], Response::HTTP_OK);
    }

    #[Route('/{post}/keep-suggets', name: 'suggets_keep_post', methods: ['POST'])]
    /**
     * @OA\Tag(name="Post - Keeping")
     */
    public function suggetsKeep(Post $post): JsonResponse
    {
        if (!$post->getKeepedBy() && !$post->isKeepingValidate()) {
            $post->setKeepedBy($this->getUser())
                ->setKeepingValidate(false);

            $this->entityManager->persist($post);
            $this->entityManager->flush();

            $this->mailerService->createEmail($post->getPostedBy()->getEmail(), "PROPOSITION DE GARDIENNAGE");
            $this->mailerService->setBodyWithTemplate(
                'emails/posts/propose_keeping.html.twig',
                [
                    'keeper' => $post->getKeepedBy(),
                    'user' => $post->getPostedBy(),
                    'post' => $post
                ]
            );
            $this->mailerService->sendEmail();
            foreach ($post->getImages() as $image) {
                $image->setName($this->helper->asset($image, 'file'));
                $post->addImage($image);
            }
            return new JsonResponse($this->normalizer->normalize($post, 'json', ['groups' => ['post', 'comment']]), Response::HTTP_OK);
        }
        return new JsonResponse(['message' => 'Un utilisateur sest déja proposé'], Response::HTTP_CONFLICT);
    }

    #[Route('/{post}/keep', name: 'validate_keep_post', methods: ['POST'])]
    /**
     * @OA\Tag(name="Post - Keeping")
     */
    public function validKeeping(Request $request, Post $post): JsonResponse
    {
        if ($request->request->get('isValidated') !== null) {
            if ($request->request->get('isValidated') == 'validated') {
                $post->setKeepingValidate(true);
                $keeper = $post->getKeepedBy();
            } else {
                $keeper = $post->getKeepedBy();
                $post->setKeepedBy(null);
            }

            $this->entityManager->persist($post);
            $this->entityManager->flush();

            $this->mailerService->createEmail($keeper->getEmail(), "L'UTILISATEUR A REPONDU A VOTRE DEMANDE");
            $this->mailerService->setBodyWithTemplate(
                'emails/posts/reponse_keeping.html.twig',
                [
                    'keeper' => $keeper ?? $post->getKeepedBy(),
                    'user' => $post->getPostedBy(),
                    'post' => $post,
                    'validate' => $request->request->get('isValidated') === 'validated'
                ]
            );
            $this->mailerService->sendEmail();
            foreach ($post->getImages() as $image) {
                $image->setName($this->helper->asset($image, 'file'));
                $post->addImage($image);
            }
            return new JsonResponse($this->normalizer->normalize($post, 'json', ['groups' => ['post', 'comment']]), Response::HTTP_OK);
            // message automatique sur l'app en mode bjr j'ai validé votre proposition
        }
        return new JsonResponse(['message' => 'Une erreur est survenu'], Response::HTTP_CONFLICT);
    }
}
