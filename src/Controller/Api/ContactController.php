<?php

namespace App\Controller\Api;

use App\Form\ContactType;
use App\Service\MailerService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    public const SEND_TO = 'contact@arosaje.fr';

    #[Route('/contact', name: 'contact', methods: ['POST'])]
    public function contact(Request $request, MailerService $mailerService)
    {
        $form = $this->createForm(ContactType::class);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            // envoie de l'email pour nous
            $mailerService->createEmail(self::SEND_TO, "CONTACT UTILISATEUR");
            $mailerService->setBodyWithTemplate('emails/contact/contact.html.twig', ['data' => $form->getData()]);
            $mailerService->sendEmail();

            // envoie l'email de remerciement
            $mailerService->createEmail($form->getData()['email'], "MERCI POUR VOTRE CONTACT");
            $mailerService->setBodyWithTemplate('emails/contact/contact_response.html.twig', ['data' => $form->getData()]);
            $mailerService->sendEmail();

            return new JsonResponse(['message' => 'email send'], Response::HTTP_OK);
        }
        return new JsonResponse(['message' => 'email not send form not valide'], Response::HTTP_CONFLICT);
    }
}
