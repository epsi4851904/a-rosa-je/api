<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

#[Route('/profil')]
/**
 * @OA\Tag(name="User")
 */
class UserController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
        private readonly NormalizerInterface $normalizer,
        private readonly UploaderHelper $helper,
        private readonly MailerService $mailerService
    ) {
    }

    #[Route('/update', name: 'update_user', methods: ['POST'])]
    public function update(Request $request): JsonResponse
    {
        $user = $this->userRepository->find($this->getUser()->getId());
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return new JsonResponse(
                $this->normalizer->normalize($user, 'json', ['groups' => 'profile']),
                Response::HTTP_OK
            );
        }
        return new JsonResponse(['message' => 'Une erreur est survenue'], Response::HTTP_CONFLICT);
    }

    #[Route('/data-export', name: 'export_data_user', methods: ['GET'])]
    public function exportDataUser(): JsonResponse
    {
        $user = $this->userRepository->findOneBy(['id' => $this->getUser()->getId()]);
        $jsonData = json_encode([
            'donnée utilisateur' => $this->normalizer->normalize($user, 'json', ['groups' => ['profile']]),
            'vos posts' => $this->normalizer->normalize($user->getPosts(), 'json', ['groups' => ['post']]),
            'vos commentaires' => $this->normalizer->normalize($user->getComments(), 'json', ['groups' => ['comment']]),
            'vos messages' => $this->normalizer->normalize($user->getMessages(), 'json', ['groups' => ['message']])
        ]);
        $tempFileName = tempnam(sys_get_temp_dir(), 'json_data');
        file_put_contents($tempFileName, $jsonData);

        $this->mailerService->createEmail($user->getEmail(), "Vos données de l'application Arosaje")
            ->setBodyWithTemplate('emails/user/export-data.html.twig', ['user' => $user])
            ->addAttachments($tempFileName, 'data_export_'.$user->getFirstName().'_'.$user->getLastName().'.json')
            ->sendEmail();
        unlink($tempFileName);
        return new JsonResponse(['message' => 'email envoyé'], Response::HTTP_OK);
    }

    #[Route('/posts/{user}', name: 'get_post_by_user', methods: ['GET'])]
    public function getPostsByUser(User $user, PostRepository $postRepository): JsonResponse
    {
        if ($user->getDeletedAt() !== null) {
            return new JsonResponse(['message' => "L'utilisateur a supprimé son compte"], Response::HTTP_CONFLICT);
        }

        $posts = $postRepository->findBy(['postedBy' => $user, 'deletedAt' => null]);
        $keeping = $postRepository->findBy(['keepedBy' => $user, 'keepingValidate' => true, 'deletedAt' => null]);
        $waitingKeeped = $postRepository->getPostWithActionByUser($user);
        $waitingKeepedBy = $postRepository->findBy(['postedBy' => $user,'deletedAt' => null, 'keepedBy' => ['is not null'], 'keepingValidate' => false]);
        $waiting = array_merge($waitingKeeped, $waitingKeepedBy);

        $data = compact('posts', 'keeping', 'waiting');

        return new JsonResponse(
            $this->normalizer->normalize($data, 'json', ['groups' => ['post', 'comment']]),
            Response::HTTP_OK
        );
    }

    #[Route('/{user}', name: 'get_user_by_id', methods: ['GET'])]
    public function getUserById(User $user, PostRepository $postRepository): JsonResponse
    {
        if ($user->getDeletedAt() !== null) {
            return new JsonResponse(['message' => "L'utilisateur a supprimé son compte"], Response::HTTP_CONFLICT);
        }

        return new JsonResponse(
            $this->normalizer->normalize($user, 'json', ['groups' => ['profile']]),
            Response::HTTP_OK
        );
    }

    #[Route('/{user}', name: 'delete_user', methods: ['DELETE'])]
    public function delete(User $user): JsonResponse
    {
        $user->setDeletedAt(new \DateTimeImmutable());
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new JsonResponse(['message' => 'Votre compte a bien été supprimé'], Response::HTTP_OK);
    }
}
