<?php

namespace App\Controller\Api;

use App\Entity\Comment;
use App\Entity\Post;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/comment')]
/**
 * @OA\Tag(name="Commentaire")
 */
class CommentController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly NormalizerInterface $normalizer
    ) {
    }

    #[Route('/{post}', name: 'app_new_comment', methods: ['POST'])]
    public function create(Request $request, Post $post, Security $security, CommentRepository $commentRepository): JsonResponse
    {
        if (!$security->isGranted('ROLE_BOTANISTE')) {
            return new JsonResponse(['message' => "Vous n'êtes pas autorisée"], Response::HTTP_FORBIDDEN);
        }

        if ($request->request->get('comment')) {
            $comment = new Comment();
            $comment->setText($request->request->get('comment'))
                ->setPost($post)
                ->setPostedBy($this->getUser());

            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            $comments = $commentRepository->findBy(['post' => $comment->getPost()]);

            return new JsonResponse(
                $this->normalizer->normalize($comments, 'json', ['groups' => 'comment']),
                Response::HTTP_OK
            );
        }
        return new JsonResponse(['message' => 'Commentaire manquant'], Response::HTTP_CONFLICT);
    }

    #[Route('/{comment}/update', name: 'app_update_comment', methods: ['POST'])]
    public function update(Request $request, Comment $comment, Security $security): JsonResponse
    {
        if (!$security->isGranted('ROLE_BOTANISTE')) {
            return new JsonResponse(['message' => "Vous n'êtes pas autorisée"], Response::HTTP_FORBIDDEN);
        }

        if ($request->request->get('comment')) {
            $comment->setText($request->request->get('comment'));

            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            return new JsonResponse(['message' => 'Commentaire modifié'], Response::HTTP_OK);
        }
        return new JsonResponse(['message' => 'Commentaire manquant'], Response::HTTP_CONFLICT);
    }

    #[Route('/{comment}', name: 'delete_comment_by_id', methods: ['DELETE'])]
    public function deletePostById(Comment $comment): JsonResponse
    {
        $comment->setDeletedAt(new \DateTime());

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return new JsonResponse(['message' => 'Comment deleted'], Response::HTTP_OK);
    }
}
