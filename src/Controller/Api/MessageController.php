<?php

namespace App\Controller\Api;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/message')]
/**
 * @OA\Tag(name="Message")
 */
class MessageController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly NormalizerInterface $normalizable,
        private readonly MessageRepository $messageRepository,
        private readonly UserRepository $userRepository
    ) {
    }

    #[Route('', name: 'get_all_message', methods: ['GET'])]
    public function list():JsonResponse
    {
        $data = [];
        $users = $this->messageRepository->getAllMessage($this->getUser());
        foreach ($users as $userId) {
            $user = $this->userRepository->find($userId['id']);
            $messages = $this->messageRepository->getMessages($user, $this->getUser());
            $messages = array_filter($messages, function (Message $message) {
                return $message->getViewAt() === null && $message->getRecipient() === $this->getUser();
            });
            $data[] = [
                'user' => $user,
                'messages' => $messages,
                'nbNewMessage' => count($messages),
                'isSend' => isset($messages[0]) && $messages[0]->getSender()->getId() === $this->getUser()->getId(),
                'last' => $this->messageRepository->getLastMessage($user, $this->getUser())
            ];
        }
        return new JsonResponse($this->normalizable->normalize($data, 'json', ['groups' => ['profile', 'message']]), Response::HTTP_OK);
    }

    #[Route('/send/{user}', name: 'send_message_at_user', methods: ['POST'])]
    public function send(Request $request, User $user, HubInterface $hub): JsonResponse
    {
        if ($request->request->get('message')) {
            $message = new Message();
            $message->setRecipient($user)
                ->setSender($this->getUser())
                ->setText($request->request->get('message'));
            $this->entityManager->persist($message);
            $this->entityManager->flush();

            $update = new Update(
                'message/' . $message->getRecipient()->getId() . '/' . $message->getSender()->getId(),
                json_encode($this->normalizable->normalize($message, 'json', ['groups' => ['profile', 'message']]))
            );

            $hub->publish($update);

            return new JsonResponse($this->normalizable->normalize($message, 'json', ['groups' => ['profile', 'message']]), Response::HTTP_OK);
        }
        return new JsonResponse(['message' => 'pas de text rentré'], Response::HTTP_CONFLICT);
    }

    #[Route('/view/{messages}', name: 'view_message_by_id', methods: ['POST'])]
    public function view(string $messages): JsonResponse
    {
        $messages = explode(',', $messages);
        foreach ($messages as $messageId) {
            /** @var Message $message */
            $message = $this->messageRepository->find($messageId);
            if ($message->getRecipient() === $this->getUser()) {
                $message->setViewAt(new \DateTimeImmutable());

                $this->entityManager->persist($message);
                $this->entityManager->flush();
                continue;
            }
            return new JsonResponse(['message' => "Vous n'etes pas le destinataire de ce message"], Response::HTTP_CONFLICT);
        }
        return new JsonResponse(['message' => 'message bien vu'], Response::HTTP_OK);
    }

    #[Route('/{recipient}', name: 'get_messages_with_user', methods: ['GET'])]
    public function getMessageWithUser(User $recipient): JsonResponse
    {
        $messages = $this->messageRepository->getMessages($recipient, $this->getUser());

        return new JsonResponse($this->normalizable->normalize($messages, 'json', ['groups' => 'message']), Response::HTTP_OK);
    }
}
