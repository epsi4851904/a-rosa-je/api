<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class AuthenticationController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly MailerService $mailerService,
        private readonly JWTEncoderInterface $JWTEncoder
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route('/register', name: 'app_registration', methods: ['POST'])]
    /**
     * @OA\Tag(name="Registration")
     */
    public function register(Request $request, ValidatorInterface $validator, UserPasswordHasherInterface $passwordHasher): Response
    {
        $form = $this->createForm(RegistrationType::class);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            if ($this->userRepository->findBy(['email' => $user->getEmail()])) {
                return $this->json(['success' => false, 'message' => 'Cet e-mail est déjà utilisé.'], Response::HTTP_CONFLICT);
            }
            /** @var User $user */
            $hashedPassword = $passwordHasher->hashPassword($user, $data['password']);
            $user->setPassword($hashedPassword);
            $user->setCodeValidation((int)str_pad((string)mt_rand(0, 9999), 4, '0', STR_PAD_LEFT));
            $user->setRoles(['ROLE_USER']);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $subject = 'VALIDATION D\'INSCRIPTION';
            $this->mailerService->createEmail($user->getEmail(), $subject)
                ->setBodyWithTemplate('emails/security/signup.html.twig', ['user' => $user])
                ->sendEmail();

            $token = $this->JWTEncoder->encode([
                'username' => $user->getEmail()
            ]);

            return $this->json([
                'status' => Response::HTTP_CREATED,
                'message' => 'Inscription réussie.',
                'token' => $token,
                'code' => $user->getCodeValidation()
            ], Response::HTTP_CREATED);
        }
        return new JsonResponse(['success' => false, 'message' => 'Une erreur s\'est produite'], Response::HTTP_CONFLICT);
    }

    #[Route('/verif-code', name: 'app_verif_code', methods: ['POST'])]
    /**
     * @OA\Tag(name="Registration")
     */
    public function verifCode(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());
        /** @var User $user */
        $user = $this->getUser();
        if ($user->isIsValidated()) {
            return new JsonResponse(['success' => false, 'message' => 'Compte déja validé'], Response::HTTP_CONFLICT);
        }
        if ($data->code !== null && (int)$data->code !== (int)$user->getCodeValidation()) {
            return new JsonResponse(['success' => false, 'message' => 'code non valide'], Response::HTTP_CONFLICT);
        }
        $user->setIsValidated(true);
        $user->setCodeValidation(null);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new JsonResponse(['success' => true, 'message' => 'code valide'], Response::HTTP_ACCEPTED);
    }

    #[Route('/reset-password', name: 'app_reset_password', methods: ['POST'])]
    public function resetPassword(Request $request, UserPasswordHasherInterface $passwordHasher)
    {
        $isChange = false;
        $token = substr($request->query->get('token'), 0, -4);
        $code = substr($request->query->get('token'), -4);
        $user = $this->userRepository->findOneBy([
            'email' => base64_decode($token),
            'codeValidation' => $code,
            'isValidated' => false,
        ]);
        if (!$user) {
            $isChange = true;
        }
        if ($_POST) {
            if ($request->request->get('password') && $request->request->get('confirm_password')) {
                if ($request->request->get('password') === $request->request->get('confirm_password')) {
                    $user->setPassword($passwordHasher->hashPassword($user, $request->request->get('password')));
                    $user->setCodeValidation(null);
                    $user->setIsValidated(true);
                    $isChange = true;

                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                }
            }
        }
        return $this->render('security/reset_password.html.twig', [
            'user' => $user,
            'isChange' => $isChange
        ]);
    }
}
