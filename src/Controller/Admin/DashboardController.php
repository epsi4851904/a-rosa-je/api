<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/dashboard', name: 'dashboard')]
    public function index(): Response
    {
        return $this->render('admin/dashboard/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Arosaje');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('Administration');
        yield MenuItem::subMenu('Gestion utilisateur', 'fa-solid fa-user-group')->setSubItems([
            MenuItem::linkToCrud('List utilisateurs', 'fa fa-user-gear', User::class),
            MenuItem::linkToCrud('Créer utilisateur', 'fa fa-user-plus', User::class)
                ->setAction('new')
        ]);

        yield MenuItem::section('Performance');
        yield MenuItem::linkToRoute('Monitoring', 'fa-solid fa-right-left', 'admin_monitoring');
        yield MenuItem::linkToRoute('Statistiques', 'fa-solid fa-chart-pie', 'admin_monitoring');
    }
}
