<?php

namespace App\Controller\Admin;

use App\Service\Chart\Monitoring\MonitoringHeaderData;
use App\Service\ChartService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MonitoringController extends AbstractDashboardController
{
    public function __construct(
        private readonly ChartService $chartService,
        private readonly MonitoringHeaderData $monitoringHeaderData
    ) {
    }

    #[Route('/monitoring', name: 'monitoring')]
    public function index(): Response
    {
        $timeRequestByMethod = $this->chartService->getChart('timeRequestByMethod');
        $nbRequestByMethod = $this->chartService->getChart('nbRequestByMethod');
        $allRequestTime = $this->chartService->getChart('allRequestTime');

        return $this->render('admin/monitoring/index.html.twig', [
            'headerData' => $this->monitoringHeaderData->getData(),
            'timeRequestByMethod' => $timeRequestByMethod,
            'nbRequestByMethod' => $nbRequestByMethod,
            'allRequestTime' => $allRequestTime
        ]);
    }
}
