<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ArrayFilter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MailerService $mailerService
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ($entityInstance instanceof User) {
            $entityInstance->setCodeValidation((int)str_pad((string)mt_rand(0, 9999), 4, '0', STR_PAD_LEFT));

            $subject = 'MODIFICATION MOT DE PASSE';
            $url = 'http://localhost:8000/api/reset-password?token='.base64_encode($entityInstance->getEmail()) . $entityInstance->getCodeValidation();
            $this->mailerService->createEmail($entityInstance->getEmail(), $subject)
                ->setBodyWithTemplate('emails/security/reset-password.html.twig', ['user' => $entityInstance, 'url' => $url])
                ->sendEmail();
            $this->entityManager->persist($entityInstance);
            $this->entityManager->flush();
        }
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('firstName'),
            TextField::new('lastName'),
            TextField::new('email'),
            TextField::new('password')->hideOnIndex(),
            ArrayField::new('roles')
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(ArrayFilter::new('roles')->setFormTypeOption('value_type_options', ['choices' => ['Administrateur' => 'ROLE_ADMIN', 'Botaniste' => 'ROLE_BOTANISTE' ]]))
            ;
    }
}
