<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[Gedmo\Loggable()]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?string $text = null;

    #[ORM\Column]
    #[Gedmo\Timestampable(on: 'create')]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?\DateTimeImmutable $sendAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?\DateTimeImmutable $viewAt = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?User $recipient = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    #[Groups(['message'])]
    #[Gedmo\Versioned]
    private ?User $sender = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): static
    {
        $this->text = $text;

        return $this;
    }

    public function getSendAt(): ?\DateTimeImmutable
    {
        return $this->sendAt;
    }

    public function setSendAt(\DateTimeImmutable $sendAt): static
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    public function getViewAt(): ?\DateTimeImmutable
    {
        return $this->viewAt;
    }

    public function setViewAt(?\DateTimeImmutable $viewAt): static
    {
        $this->viewAt = $viewAt;

        return $this;
    }

    public function getRecipient(): ?User
    {
        return $this->recipient;
    }

    public function setRecipient(?User $recipient): static
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(?User $sender): static
    {
        $this->sender = $sender;

        return $this;
    }
}
