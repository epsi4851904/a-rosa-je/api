<?php

namespace App\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RequestResponseLoggerMiddleware implements HttpKernelInterface
{
    public function __construct(private readonly HttpKernelInterface $app, private readonly string $csvFilename)
    {
    }
    public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = true): Response
    {
        $startTime = microtime(true);

        $response = $this->app->handle($request, $type, $catch);

        $responseTime = microtime(true) - $startTime;

        $url = $request->getUri();
        $ip = $request->getClientIp();
        $method = $request->getMethod();
        $httpStatus = $response->getStatusCode();
        $responseSize = strlen($response->getContent());
        $userAgent = $request->headers->get('User-Agent');
        $dateTime = date('Y-m-d H:i:s');
        $data = [
            $dateTime,
            $url,
            $ip,
            $method,
            $httpStatus,
            $responseTime,
            $responseSize,
            $userAgent
        ];
        $this->writeToCSV($data);

        return $response;
    }

    private function writeToCSV(array $data)
    {
        if (!file_exists($this->csvFilename)) {
            $csvFile = fopen($this->csvFilename, 'w');
            fputcsv($csvFile, ['Date', 'URL', 'IP', 'Method', 'HTTP Status', 'Response Time', 'Response Size', 'User-Agent']);
            fclose($csvFile);
        }

        $csvFile = fopen($this->csvFilename, 'a');
        fputcsv($csvFile, $data);
        fclose($csvFile);
    }
}
