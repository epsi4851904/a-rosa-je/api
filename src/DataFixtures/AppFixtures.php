<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();

        $user->setEmail('contact@arosaje.fr')
            ->setPassword($this->passwordHasher->hashPassword($user, 'Arosaje1234@'))
            ->setRoles(['ROLE_USER'])
            ->setLastName('admin')
            ->setFirstName('admin');

        $image = new Image();
        $image->setName('image_test.png')
            ->setIsMain(true)
            ->setSize(350);

        $post = new Post();
        $post->setPostedBy($user)
            ->setZipcode(33000)
            ->setCity('Bordeaux')
            ->setDescription('description de la plante')
            ->setNamePlant('orchidée')
            ->setStartAt(new \DateTime())
            ->setEndAt(new \DateTime())
            ->addImage($image);

        $manager->persist($user);
        $manager->persist($image);
        $manager->persist($post);
        $manager->flush();
    }
}
