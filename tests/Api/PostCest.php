<?php

namespace App\Tests\Api;

use App\Tests\Support\ApiTester;
use Codeception\Template\Api;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PostCest
{
    public function _before(ApiTester $I): void
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/api/login_check', json_encode([
            'username' => 'contact@arosaje.fr',
            'password' => 'Arosaje1234@'
        ]));

        $response = json_decode($I->grabResponse(), true);
        $token = $response['token'];
        $I->amBearerAuthenticated($token);
    }

    public function createPostSuccess(ApiTester $I)
    {
        $formData = [
            'namePlant' => 'Orchidée',
            'description' => 'petite plante qui ets mal en point, j\'aimerais avoir des conseils pour quelle aille mieux ',
            'city' => 'Bordeaux',
            'zipcode' => '33000',
            'latitude' => '48.8566',
            'longitude' => '2.3522',
            'startAt' => '2024-03-01T15:17:57',
            'endAt' => '2024-03-05T15:17:57',
        ];
        $filePath = codecept_data_dir('image_test.png');

        $uploadedFile = new UploadedFile(
            $filePath,
            'image_test.png',
            'image/png',
            null,
            true
        );

        $I->haveHttpHeader('Content-Type', 'multipart/form-data; boundary=---011000010111000001101001');
        $I->sendPOST('/api/post', $formData, ['image' => $uploadedFile]);

        $I->seeResponseCodeIs(201);
        $I->seeResponseContainsJson([
            'message' => 'Le post a bien été créée'
        ]);
    }

    public function getPostSuccess(ApiTester $I)
    {
        $I->sendGet('/api/post');
        $I->seeResponseCodeIs('200');
    }
}