<?php


namespace App\Tests\Api;

use App\Tests\Support\ApiTester;

class RegistrationCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    // tests

    public function loginSuccess(ApiTester $I): void
    {
        $I->sendPOST('/api/login_check', json_encode([
            'username' => 'contact@arosaje.fr',
            'password' => 'Arosaje1234@'
        ]));

        $I->seeResponseCodeIs(200);
        $I->seeResponseJsonMatchesJsonPath('$.token');
    }

    public function loginWithInvalidCredentials(ApiTester $I): void
    {
        $I->sendPOST('/api/login_check', json_encode([
            'username' => 'contact@arosaje.fr',
            'password' => 'MauvaisMDP'
        ]));

        $I->seeResponseCodeIs(401);
        $I->seeResponseContainsJson(['message' => 'Invalid credentials.']);
    }

    public function registerSuccess(ApiTester $I)
    {
        $I->sendPOST('/api/register', [
            'lastName' => 'matteo',
            'firstName' => 'bonneval',
            'email' => 'matteo.bonneval@dotsafe.fr',
            'password' => 'Matteo1234@',
        ]);

        $I->seeResponseCodeIs(201);
        $I->seeResponseJsonMatchesJsonPath('$.token');
    }

    public function registerWithMailAlreadyExist(ApiTester $I)
    {
        $I->sendPOST('/api/register', [
            'lastName' => 'matteo',
            'firstName' => 'bonneval',
            'email' => 'contact@arosaje.fr',
            'password' => 'toto'
        ]);

        $I->seeResponseCodeIs(409);
        $I->seeResponseContainsJson(['success' => false]);
        $I->seeResponseContainsJson(['message' => 'Cet e-mail est déjà utilisé.']);
    }

    public function registerWithConflict(ApiTester $I)
    {
        $I->sendPOST('/api/register', [
            'lastNam' => 'matteo',
            'firstName' => 'bonneval',
            'email' => 'contact@arosaje.fr',
            'password' => 'toto'
        ]);

        $I->seeResponseCodeIs(409);
        $I->seeResponseContainsJson(['success' => false]);
    }

    public function validationCodeAccountSuccess(ApiTester $I)
    {
        $I->sendPOST('/api/register', [
            'lastName' => 'matteo',
            'firstName' => 'bonneval',
            'email' => 'matteo.bonneval@dotsafe.fr',
            'password' => 'Matteo1234@',
        ]);

        $I->seeResponseCodeIs(201);
        $I->seeResponseJsonMatchesJsonPath('$.token');

        $response = json_decode($I->grabResponse(), true);
        $token = $response['token'];
        $code = $response['code'];
        $I->amBearerAuthenticated($token);

        $I->sendPOST('/api/verif-code', ['code' => $code]);

        $I->seeResponseCodeIs(202);
        $I->seeResponseContainsJson(['success' => true]);
    }

    public function validationCodeAccountWithCodeInvalid(ApiTester $I)
    {
        $I->sendPOST('/api/register', [
            'lastName' => 'matteo',
            'firstName' => 'bonneval',
            'email' => 'matteo.bonneval@dotsafe.fr',
            'password' => 'Matteo1234@',
        ]);

        $I->seeResponseCodeIs(201);
        $I->seeResponseJsonMatchesJsonPath('$.token');

        $response = json_decode($I->grabResponse(), true);
        $token = $response['token'];
        $I->amBearerAuthenticated($token);

        $I->sendPOST('/api/verif-code', ['code' => 1111]);

        $I->seeResponseCodeIs(409);
        $I->seeResponseContainsJson(['success' => false]);
    }
}