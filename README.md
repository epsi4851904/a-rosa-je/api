# API

# Pour installer le projet
<span style="color: red">**il vous faudra docker installé**</span>

```shell
git clone https://gitlab.com/epsi4851904/a-rosa-je/api.git

cd api
# Build l'image docker
docker compose build
# Lancer les containers docker
docker compose up -d
# Création d'un bash dans le container php
docker exec -it api_a_rosa_je bash
# Installer les composants si nécessaire
composer install
# Faire les migrations
php bin/console doctrine:migrations:migrate
# Création des clés jwt
php bin/console lexik:jwt:generate-keypair
```

# Pour lancer le projet

````shell
docker compose up -d
````

# Important

**Pour toute manipulation de l'api, faire les commandes dans un terminal du container**\
commande pour ouvrir un terminal :
``docker exec -it api_a_rosa_je bash``

## api

http://localhost:8000

http://localhost:8000/api

## phpMyAdmin

http://localhost:8080

identifiant pour se connecter :
```
identifiant: user
mot de passe: password
nom bdd: a-rosa-je
```

## maildev 

http://localhost:1080/#/