# Utilisez une image PHP Apache
FROM php:8.2-apache-buster

# Installe les dépendances nécessaires
RUN apt-get update && apt-get install -y \
    libicu-dev \
    zip \
    unzip \
    && docker-php-ext-install pdo_mysql intl

# Installe Node.js
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs

# Installe Yarn
RUN npm install -g yarn


# Copie le code source de l'application Symfony dans le conteneur
COPY . /var/www/html

COPY apache.conf /etc/apache2/sites-available/000-default.conf

# Définit le répertoire de travail
WORKDIR /var/www/html

# Installe les dépendances Symfony
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-plugins --no-scripts


# Active le module Apache mod_rewrite
RUN a2enmod rewrite headers
